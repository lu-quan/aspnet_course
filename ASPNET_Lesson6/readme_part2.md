//原生用法
SqlConnection conn = new SqlConnection
{
    ConnectionString = @"Data Source=.\SQLEXPRESS;Integrated Security=True;DataBase=Students_New"
};
conn.Open();

SqlCommand cmd = new SqlCommand
{
    Connection = conn,

    CommandText = "select * from StudentInfo"
};

var red = cmd.ExecuteReader();

Repeater1.DataSource = red;
Repeater1.DataBind();

//Dapper

IDbConnection connection = new SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=Students_New;Integrated Security=True;MultipleActiveResultSets=True");

var data = connection.Query<StudentInfo>("select * from StudentInfo");

Repeater1.DataSource = data;
Repeater1.DataBind();