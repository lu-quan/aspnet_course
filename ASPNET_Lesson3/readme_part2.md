一、方法的定义

在我们学习 C# 语言的过程中会发现之前的应用程序中默认会生成一个主方法 Main()，它是执行程序的入口和出口。

方法是将完成同一功能的内容放到一起，方便书写和调用的一种方式，也体现了面向对象语言中封装的特性。

定义方法的语法形式如下。

访问修饰符    修饰符    返回值类型    方法名(参数列表)
{
    语句块;
}

其中：
1) 访问修饰符
所有类成员访问修饰符都可以使用，如果省略访问修饰符，默认是 private。
2) 修饰符
在定义方法时修饰符包括 virtual（虚拟的）、abstract（抽象的）、override（重写的）、static（静态的）、sealed（密封的）。override 是在类之间继承时使用的。
3) 返回值类型
用于在调用方法后得到返回结果，返回值可以是任意的数据类型，如果指定了返回值类型，必须使用 return 关键字返回一个与之类型匹配的值。如果没有指定返回值类型，必须使用 void 关键字表示没有返回值。
4) 方法名
对方法所实现功能的描述。方法名的命名是以 Pascal 命名法为规范的。
5)参数列表
在方法中允许有 0 到多个参数，如果没有指定参数也要保留参数列表的小括号。参数的定义形式是“数据类型参数名”，如果使用多个参数，多个参数之间需要用逗号隔开。


namespace code_1
{
    class Test
    {
        private int id;                         //定义私有的整型字段 id
        public readonly string name;            //定义公有的只读字符串类型字段 name
        internal static int age;                //定义内部的静态的整型字段 age
        private const string major = "计算机";  //定义私有的字符串类型常量 major
        private void PrintMsg()
        {
            Console.WriteLine("编号：" + id);
            Console.WriteLine("姓名：" + name);
            Console.WriteLine("年龄：" + age);
            Console.WriteLine("专业：" + major);
        }
    }
}


练习：创建 Compute 类，分别定义 4 个方法实现加法、减法、乘法、除法的操作。



二、get和set访问器：获取和设置字段（属性）的值


属性经常与字段连用，并提供了 get 访问器和 set 访问器，分别用于获取或设置字段的值。

get 访问器和 set 访问器的使用与方法非常类似，可以在操作字段时根据一些规则和条件来设置或获取字段的值。

此外，为了保证字段的安全性，还能选择省去 get 访问器或 set 访问器。

定义属性的语法形式如下。
public    数据类型    属性名
{
    get
    {
        获取属性的语句块;
        return 值;
    }
    set
    {
        设置属性得到语句块;
    }
}

其中：
1) get{}
get 访问器，用于获取属性的值，需要在 get 语句最后使用 return 关键字返回一个与属性数据类型相兼容的值。

若在属性定义中省略了该访问器，则不能在其他类中获取私有类型的字段值，因此也称为只写属性。
2) set{}
set 访问器用于设置字段的值，这里需要使用一个特殊的值 value，它就是给字段赋的值。

在 set 访问器省略后无法在其他类中给字段赋值，因此也称为只读属性。

通常属性名的命名使用的是 Pascal 命名法，单词的首字母大写，如果是由多个单词构成，每个单词的首字母大写。

由于属性都是针对某个字段赋值的，因此属性的名称通常是将字段中每个单词的首字母大写。例如定义了一个名为 name 的字段，属性名则为 Name。


练习：定义一个图书信息类（Book），在类中定义图书编号（id）、图书名称（name）、图书价格（price）3 个字段，并分别为这 3 个字段设置属性，其中将图书名称设置为只读属性。

namespace code_1
{
    class Book
    {
        private int id;
        private string name;
        private double price;
        //设置图书编号属性
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        
        //设置图书名称属性
        public string Name
        {
            get
            {
                return name;
            }
        }
        //设置图书价格属性
        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }
    }
}


在定义字段属性时，属性的作用就是为字段提供 get、set 访问器，由于操作都比较类似，在 C# 语言中可以将属性的定义简化成如下写法。
public    数据类型    属性名{get;set;}


这种方式也被称为自动属性设置。简化后图书类中的属性设置的代码如下。

public int Id{get; set;}
public string Name{get; set;}
public double Price{get; set;}

如果使用上面的方法来设置属性，则不需要先指定字段。如果要使用自动属性的方式来设置属性表示只读属性，直接省略 set 访问器即可。只读属性可以写成如下形式。

public int Id{get;}=1;

这里相当于将 Id 属性的值设置成 1，并且要以分号结束。但是，在使用自动生成属性的方法时不能省略 get 访问器，如果不允许其他类访问属性值，则可以在 get 访问器前面加上访问修饰符 private，代码如下。

public int Id{private get; set;}
这样，Id 属性的 get 访问器只能在当前类中使用。