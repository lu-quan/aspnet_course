string字符串详解


在编程时字符串是比较常用的一种数据类型，例如用户名、邮箱、家庭住址、商品名称等信息都需要使用字符串类型来存取。

在 C# 语言中提供了对字符串类型数据操作的方法，例如截取字符串中的内容、查找字符串中的内容等。

在任何一个软件中对字符串的操作都是必不可少的，掌握好字符串的操作将会在编程中起到事半功倍的作用。


一、字符串及常用方法


常用的字符串操作包括获取字符串的长度、查找某个字符在字符串中的位置、替换字符串中的内容、拆分字符串等。

在字符串操作中常用的属性或方法如下表所示。

编号	属性或方法名	作用
1	Length	获取字符串的长度，即字符串中字符的个数
2	IndexOf	返回整数，得到指定的字符串在原字符串中第一次出现的位置
3	LastlndexOf	返回整数，得到指定的字符串在原字符串中最后一次出现的位置
4	Starts With	返回布尔型的值，判断某个字符串是否以指定的字符串开头
5	EndsWith	返回布尔型的值，判断某个字符串是否以指定的字符串结尾
6	ToLower	返回一个新的字符串，将字符串中的大写字母转换成小写字母
7	ToUpper	返回一个新的字符串，将字符串中的小写字母转换成大写字母
8	Trim	返回一个新的字符串，不带任何参数时表示将原字符串中前后的空格删除。 参数为字符数组时表示将原字符串中含有的字符数组中的字符删除
9	Remove	返回一个新的字符串，将字符串中指定位置的字符串移除
10	TrimStart	返回一个新的字符串，将字符串中左侧的空格删除
11	TrimEnd	返回一个新的字符串，将字符串中右侧的空格删除
12	PadLeft	返回一个新的字符串，从字符串的左侧填充空格达到指定的字符串长度
13	PadRight	返回一个新的字符串，从字符串的右侧填充空格达到指定的字符串长度
14	Split	返回一个字符串类型的数组，根据指定的字符数组或者字符串数组中的字符 或字符串作为条件拆分字符串
15	Replace	返回一个新的字符串，用于将指定字符串替换给原字符串中指定的字符串
16	Substring	返回一个新的字符串，用于截取指定的字符串
17	Insert	返回一个新的字符串，将一个字符串插入到另一个字符串中指定索引的位置
18	Concat	返回一个新的字符串，将多个字符串合并成一个字符串


1）获取字符串长度（string.Length）
字符串实际上是由多个字符组成的，字符串中的第一个字符使用字符串[0]即可得。[0]中的 0 称为下标。

获取字符串中的第一个字符使用的下标是 0,则字符串中最后一个字符的下标是字符串的长度减 1。

使用 C# 语言如果要获取字符串的长度，使用Length属性即可，获取的方法如下。
字符串.Length

【实例 1】在 Main 方法中从控制台输入一个字符串，输出该字符串的长度，以及字符串中的第一个字符和最后一个字符。

根据题目要求，代码如下。
class Program
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        Console.WriteLine("字符串的长度为：" + str.Length);
        Console.WriteLine("字符串中第一个字符为：" + str[0]);
        Console.WriteLine("字符串中最后一个字符为：" + str[str.Length - 1]);
    }
}


从该实例可以看出，获取字符串中的某个字符只需要通过下标即可完成。

【实例 2】在 Main 方法中从控制台输入一个字符串，并将字符串中的字符逆序输出。

根据题目要求，代码如下。
class Program
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        for(int i = str.Length - 1; i >= 0; i--)
        {
            Console.WriteLine(str[i]);
        }
    }
}


2）IndexOf和LastIndexOf：查找字符串中的字符


在字符串中查找是否含有某个字符串是常见的一个应用，例如在输入的字符串中查找特殊字符、获取某个字符串在原字符串中的位置等。

在 C# 中字符串的查找方法有 IndexOf、LastlndexOf，IndexOf 方法得到的是指定字符串在原字符串中第一次出现的位置，LastlndexOf 方法得到的是指定字符串在查找的字符串中最后一次出现的位置。

需要注意的是字符串中的每个字符的位置是从 0 开始的。

无论是哪个方法，只要指定的字符串在查找的字符串中不存在，结果都为 -1。

如果要判断字符串中是否仅含有一个指定的字符串，则需要将 IndexOf 和 LastlndexOf 方法一起使用，只要通过这两个方法得到的字符串出现的位置是同一个即可。

【实例 1】在 Main 方法中从控制台输入一个字符串，然后判断字符串中是否含有 @, 并输出 @ 的位置。

根据题目要求，使用 IndexOf 方法查找的代码如下。
class Program
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        if (str.IndexOf("@") != -1)
        {
            Console.WriteLine("字符串中含有@，其出现的位置是{0}", str.IndexOf("@") + 1);
        }
        else
        {
            Console.WriteLine("字符串中不含有@");
        }
    }
}



【实例 2】在 Main 方法中从控制台输入一个字符串，判断该字符串中是否仅含有一个 @。

根据题目要求，使用 IndexOf 方法查找第一个 @ 出现的位置与使用 LastlndexOf 方法查找 @ 在字符串中最后一次出现的位置相同即可，实现的代码如下。
class Program
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        int firstIndex = str.IndexOf("@");
        int lastIndex = str.LastIndexOf("@");
        if(firstIndex != -1)
        {
            if (firstIndex == lastIndex)
            {
                Console.WriteLine("在该字符串中仅含有一个@");
            }
            else
            {
                Console.WriteLine("在该字符串中含有多个@");
            }
        }
        else
        {
            Console.WriteLine("在该字符串中不含有@");
        }
    }
}


从上面的执行效果可以看出，在字符串中包含了两个 @,因此提示的结果是“在该字符串中含有多个@”。


3）Replace：字符串替换


字符串的替换操作是指将字符串中指定的字符串替换成新字符串。

在 C# 中替换字符串的方法是Replace方法。

【实例】在 Main 方法中从控制台输入一个字符串，然后将字符串中所有的‘,’替换成‘_’。

根据题目要求，代码如下。
class Program
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        if (str.IndexOf(",") != -1)
        {
            str = str.Replace(",", "_");
        }
        Console.WriteLine("替换后的字符串为：" + str);
    }
}

 
从上面的执行效果可以看出，通过 Replace 方法将字符串中所有的‘,’换成了‘_’。


4）Substring：字符串截取


在一个字符串中截取一部分字符串也是经常用到的，例如从身份证号码中取得岀生年月日、截取手机号码的前 3 位、截取给定邮箱的用户名等。

在 C# 语言中截取字符串的方法是 Substring 方法，在使用该方法时有以下两种方法传递参数。
Substring(指定位置); //从字符串中的指定位置开始截取到字符串结束
Substring(指定位置, 截取的字符的个数); //从字符串中的指定位置开始截取指定字符个数的字符

【实例】在 Main 方法中从控制台输入邮箱，要求邮箱中仅含有一个 @，然后截取邮箱中的用户名输出。

根据题目要求，代码如下。

class Program
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        int firstIndex = str.IndexOf("@");
        int lastIndex = str.LastIndexOf("@");
        if(firstIndex != -1 && firstIndex == lastIndex)
        {
            str = str.Substring(0, firstIndex);
        }
        Console.WriteLine("邮箱中的用户名是：" + str);
    }
}


在上面的代码中，在截取邮箱中的用户名时得到 @ 的位置即可清楚需要截取的字符的个数。


5）Insert：字符串插入


在一个字符串中可以在指定位置插入另一个字符串，在 C# 中插入字符串使用的方法是 Insert 方法。

在 Insert 方法中需要两个参数，一个是字符串插入的位置，一个是字符串。

【实例】在 Main 方法中从控制台输入一个字符串，然后将‘@@@’插入到字符串的第 2 个字符的位置。

根据题目要求，代码如下。

class Program
{
    static void Main(string[] args)
    {
        string str = Console.ReadLine();
        str = str.Insert(1, "@@@");
        Console.WriteLine("新字符串为：" + str);
    }
}


从上面的执行效果可以看出，已经将‘@@@’插入到第 2 个字符的位置。



练习1：输入一串字符串，将字符串反转输出（打印）；


练习2：输入一串字符串（英文语句），将每个单词反转输出；
如：输入hello world
输出 ollhe dlrow

中文如输入：王俊
输出 俊王


练习3：输入一个身份证号，输出这个身份证号的出身日期，并判断其生日是否已经过完；


练习4：输入电子邮箱地址，判断其是否为合法的地址；
如:abc@abc.com 为合法
abc@@.abc.com为不合法

