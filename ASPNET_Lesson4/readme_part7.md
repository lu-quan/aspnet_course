lambda表达式


在C#的语法中有一种比较特殊的写法，叫做Lambda表达式，这种表达式的写法在于你查询数据的时候直接是使用以下箭头的形式来表示查询语句的：=>。例如，我们要查找学生的List<Student>集合中班级编号为1001的所有学生数据，我们即可用Studentlist.Where(t=>t.ClassCode=‘1001’)语句来直接完成，无需再写繁琐的foreach语句或者for循环。Lambda表达式的运算符即为=>。

一、Lambda表达式定义

        Lambda表达式实际上是一种匿名函数，在Lambda表达式中可以包含语句以及运算等操作。并且可用于创建委托或表达式目录树类型，支持带有可绑定到委托或表达式树的输入参数的内联表达式。使用Lambda表达式可大大减少代码量，使得代码更加的优美、简洁，更有可观性。

 

二、Lambda表达式的表现形式

       表达式形式：(Input Param)=>Expression。在表达式左侧的表示输入参数，右侧的为相应的运算语句或者判断语句等，可包含函数调用等复杂方式。运算符=>读作为goes to，例如下面这个表达t=>t.ClassCode='1001'，多做goes to ClassCode equal 1001。

       在上述的表达式中，仅仅只有当参数只有一个的时候，括号是可选择的，例如下面这种含有两个参数时候的情况应该是这样子的写法

1
(a,b)=>a==b
　　当表达式中的多个参数编译器无法自动判断类型的时候，则需要显式指定类型。

1
(int firstIndex, string str) => str.IndexOf('Hello') > firstIndex
 

三、下面介绍List集合中的Lambda表达式的运用

       在C#的List集合中，我们时常需要使用到大量的运算或者筛选等操作，按常规的方式无非就是利用foreach或者for对List集合进行循环操作，最后运算出结果。此种方法往往需要写多行语句，阅读性稍微差点，当然复杂的情况下编写也费时。博主遇到这种情况一般比较喜欢偷懒，很少会去直接写循环，而是直接使用Lambda表达式一条语句完成。

     先假定好我们待会使用的范例的格式：

     studentList对象：此对象是一个List集合，集合中的对象为学生实体Student。此集合中存放着整个学校学生的信息。

     scoreList对象：此对象是个List集合，集合中的对象是成绩实体Score，此集合中存放着为学生的成绩信息。

     Student实体：此实体包含下列几个属性，StudentName，StudentCode，ClassCode，ClassName，BirthDay，Grade。以上几个英文单词都比较简单，就不做解释了。

     Score实体：此实体包含下列几个属性，StudentCode,SubjectName(科目名称),ScoreValue(分数，0-100的数字)。一个学生可能有多门成绩数据存放在此。

    (1)查询班级编号为1001的班级下面的所有学生实体并返回到list1001中存储

1
var list1001=Studentlist.Where(t=>t.ClassCode==‘1001’)；
　(2)查询班级编号为1001的班级下面的所有学生实体并返回到list1001中存储，并按照学生的出生日期从小到大排列。

1
var list1001=Studentlist.Where(t=>t.ClassCode==‘1001’).OrderBy(t=>t.BirthDay)；
   在此说一下，OrderBy是从小到大排序，需要从大到小排列则用OrderByDescending。

　(3)查询班级编号为1001的班级下面的姓氏为【李】的同学的所有集合，并按照学生的出生日期从小到大排列。

1
var list1001=Studentlist.Where(t=>t.ClassCode==‘1001’&&t.StudentName.StartWith（“李”）).OrderBy(t=>t.BirthDay)；
　(4)查询出班级编号为1001的班级，并且存在至少一门考试科目成绩低于60分的所有同学。

1
var result = studentList.Where(t => (t.ClassCode == "1001") && (scoreList.Exists(p => p.ScoreValue < 60 && p.StudentCode == t.StudentCode)));
　　在上述语句中，lambda表达式中再次嵌入了一个lambda表达式。t参数是studentList中的lambda表达式参数，代表实体为student。p参数为scoreList中的lambda表达式参数，代表的实体为score。

   (5)其他较常用的Lambda表达式如下：


 var a = studentList.FirstOrDefault(t => t.StudentCode == "10012");//FirstOrDefault返回第一个符合条件的数据，不存在的时候返回Null。
 var b = studentList.Count(t => t.StudentName == "李世民");//返回符合条件的实体个数
 var c = studentList.FindAll(t => t.StudentName.Contains("中"));//查找所有名字中含有【中】的实体集合
 var d = studentList.GroupBy(t => t.ClassCode);//对studentList按照ClassCode分组
 var f = studentList.Max(t => t.BirthDay);//返回最大的出生日期。
 var e = scoreList.Sum(t => t.ScoreValue);//对所有成绩求和
 var g = scoreList.Average(t => t.ScoreValue);//对所有成绩求平均分
 var h = studentList.Select(t => t.StudentName).Distinct();//获取所有的学生姓名，并去除重名
